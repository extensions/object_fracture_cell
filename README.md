# Cell Fracture Add-on

This add-on was part of [Blender 4.1 bundled add-ons](https://docs.blender.org/manual/en/4.1/addons/). This is now available as an extension on the [Extensions platform](https://extensions.blender.org/add-ons/cell-fracture).

To build a new version of the extension:
* `<path_to_blender> -c extension build --source-dir=./source`

For more information about building extensions refer to the [documentation](https://docs.blender.org/manual/en/4.4/advanced/extensions/getting_started.html).

---

This add-on is offered as it is and maintaned by the community, no support expected.